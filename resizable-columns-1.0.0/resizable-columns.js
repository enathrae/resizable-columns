/*

 Version: 1.0.0
  Author: Nicolas COUVRAT
    Mail: nicolas.couvrat@mines-paristech.fr
    Repo: http://github.com/kenwheeler/slick

 */

(function ($) {
    "use strict";
    
    /* 
    EXTENDS JQUERY UI resizable PLUGIN
    */
    $.ui.plugin.add( "resizable", "alsoResizeReverse", {

        start: function() {
            var that = $( this ).resizable( "instance" ),
                o = that.options;

            $( o.alsoResizeReverse ).each( function() {
                var el = $( this );
                el.data( "ui-resizable-alsoresizeReverse", {
                    width: parseFloat( el.width() ), height: parseFloat( el.height() ),
                    left: parseFloat( el.css( "left" ) ), top: parseFloat( el.css( "top" ) )
                } );
            } );
        },

        resize: function( event, ui ) {
            var that = $( this ).resizable( "instance" ),
                o = that.options,
                os = that.originalSize,
                op = that.originalPosition,
                delta = {
                    height: ( that.size.height - os.height ) || 0,
                    width: ( that.size.width - os.width ) || 0,
                    top: ( that.position.top - op.top ) || 0,
                    left: ( that.position.left - op.left ) || 0
                };

                $( o.alsoResizeReverse ).each( function() {
                    var el = $( this ), start = $( this ).data( "ui-resizable-alsoresizeReverse" ), style = {},
                        css = el.parents( ui.originalElement[ 0 ] ).length ?
                                [ "width", "height" ] :
                                [ "width", "height", "top", "left" ];

                    $.each( css, function( i, prop ) {
                        var sum = ( start[ prop ] || 0 ) - ( delta[ prop ] || 0 );
                        if ( sum && sum >= 0 ) {
                            style[ prop ] = sum || null;
                        }
                    } );

                    el.css( style );
                } );
        },

        stop: function() {
            $( this ).removeData( "ui-resizable-alsoresize" );
        }
    } );
    
    var widget = null,
        resizeEnd;
        
    function Widget(parent, options) {

        var _ = this;
        
        _.parent = parent;
        _.horizontal = true;
        _.columns = [];
        _.columnWidth = [];
        _.columnWidthPercentage = [];
        _.lineHeight = [];
        _.lineHeightPercentage = [];
        _.numberOfColumns = 0;
        _.width = $(parent).width();
        _.height = $(parent).height();
        _.settings = $.extend({

            breakPoint: 0,
            minSize: 10,
            ignoreHidden: true

        }, options);
        
        if (_.settings.ignoreHidden) {
            
            _.columns = $(_.parent).find("div").filter(function () {
                
                return !($(this).css("display") == "none");
                
            });
            
        } else {
            
            
            _.columns = $(_.parent).find("div");
            
        }
        
        _.numberOfColumns = _.columns.length;


        _.init();

    }
    
    Widget.prototype.updateSize = function () {
        
        var _ = this;
        
        _.width = $(_.parent).width();
        _.height = $(_.parent).height();
        
    }
        
    Widget.prototype.updateWidthArray = function () {
        
        var _ = this,
            i;
        
        for (i = 0; i < _.numberOfColumns; i++) {

            _.columnWidth[i] = $(_.columns[i]).width();
            _.columnWidthPercentage[i] = (_.columnWidth[i] / _.width);

        }
        
    }
    
    Widget.prototype.updateHeightArray = function () {
        
        var _ = this,
            i;
        
        for (i = 0; i < _.numberOfColumns; i++) {
            
            _.lineHeight[i] = $(_.columns[i]).height();
            _.lineHeightPercentage[i] = (_.lineHeight[i] / _.height);
            
        }
        
    }
    
    Widget.prototype.updateColumnWidth = function () {
        
        var _ = this,
            i;
        
        for (i = 0; i < _.numberOfColumns; i++) {
            
            $(_.columns[i]).css("width", _.columnWidthPercentage[i] * _.width + 'px');
            $(_.columns[i]).css("height",'100%');
            
        }
        
    } 
    
    Widget.prototype.updateLineHeight = function () {
        
        var _ = this,
            i;
        
        for (i = 0; i < _.numberOfColumns; i++) {
            
            $(_.columns[i]).css("height", _.lineHeightPercentage[i] * _.height + 'px');
            $(_.columns[i]).css("width",'100%');
            
        }
        
    }
    
    Widget.prototype.updateMaxColumnWidth = function () {
        
        var _ = this,
            i, 
            maxColumnWidth;
        
        _.updateWidthArray();
        
        for (i = 0; i < (_.numberOfColumns - 1); i++) {

            maxColumnWidth = _.columnWidth[i] + _.columnWidth[i+1] - _.settings.minSize;
            $(_.columns[i]).resizable("option", "maxWidth", maxColumnWidth);

        }
        
    }
    
    Widget.prototype.updateMaxLineHeight = function () {
        
        var _ = this,
            i,
            maxLineHeight;
        
        _.updateHeightArray();
        
        for (i = 0; i < (_.numberOfColumns - 1); i++) {
            
            maxLineHeight = _.lineHeight[i] + _.lineHeight[i+1] - _.settings.minSize;
            $(_.columns[i]).resizable("option", "maxHeight", maxLineHeight);
            
        }
        
    }
    
    Widget.prototype.init = function () {
        
        var _ = this;
        
        if (_.width > _.settings.breakPoint) {
            
            _.initHorizontal();
            
        } else {
            
            _.initVertical();
            
        }
        
    }

    Widget.prototype.initHorizontal = function () {
        
        var _ = this,
            initialColumnWidth = _.width / _.numberOfColumns,
            initialMaxColumnWidth = 2 * initialColumnWidth - _.settings.minSize,
            resizableEW = {
            
            
                handles: "e",
                minWidth: _.settings.minSize,
                maxWidth: initialMaxColumnWidth,
                create: function () {

                    $(".ui-resizable-e").css("cursor", "ew-resize");

                },
                resize: function (event) {

                    $(_).css("left", 0);
                    event.stopPropagation();

                }
        
            },
            i;
        
        for (i = 0; i < _.numberOfColumns; i++) {
            
            $(_.columns[i])
                .addClass("resizable-column")
                .css("width", initialColumnWidth + 'px')
                .css("height", "100%");
            _.columnWidth[i] = initialColumnWidth;
            _.columnWidthPercentage[i] = 1 / _.numberOfColumns;
            
        }
        
        for (i = 0; i < (_.numberOfColumns - 1); i++) {
            
            $(_.columns[i])
                .resizable(resizableEW)
                .resizable("option", "alsoResizeReverse", $(_.columns[i+1]))
                .on('resize', function (e) {
                    
                    e.stopPropagation();

            })
                .on('resizestop', function () {
                
                _.updateMaxColumnWidth();
                
            });
    
        }
        _.horizontal = true;
    
    }
    
    Widget.prototype.initVertical = function () {
        
        var _ = this,
            initialLineHeight = _.height / _.numberOfColumns,
            initialMaxLineHeight = 2 * initialLineHeight - _.settings.minSize,
            resizableNS = {
            
            
                handles: "s",
                minHeight: _.settings.minSize,
                maxHeight: initialMaxLineHeight,
                create: function() {

                    $(".ui-resizable-s").css("cursor", "ns-resize");

                },
                resize: function (event) {

                    $(_).css("top", 0);

                }
        
            },
            i;
        
        for (i = 0; i < _.numberOfColumns; i++) {
            
            $(_.columns[i])
                .addClass("resizable-column")
                .css("height", initialLineHeight + 'px')
                .css("width", "100%");
            _.lineHeight[i] = initialLineHeight;
            _.lineHeightPercentage[i] = 1 / _.numberOfColumns;
            
        }
        
        for (i = 0; i < (_.numberOfColumns - 1); i++) {
            
            $(_.columns[i])
                .resizable(resizableNS)
                .resizable("option", "alsoResizeReverse", $(_.columns[i+1]))
                .on('resize', function (e) {
                    
                    e.stopPropagation();

            })
                .on('resizestop', function () {
                
                _.updateMaxLineHeight();
                
            });

        }
        
        _.horizontal = false;
    
    }
    
    Widget.prototype.detach = function () {
        
        var _ = this,
            i;
        
        for (i = 0; i < (_.numberOfColumns - 1); i++) {
            
            $(_.columns[i])
                .removeClass("resizable-column")
                .resizable('destroy');
            
        }
        
        $(_.columns[_.numberOfColumns]).removeClass("resizable-column");
        
        
    }
    
    Widget.prototype.destroy = function () {
        
        var _ = this;
        
        this.detach();
        
        _.columns = [];
        _.columnWidth = [];
        _.width = 0;
        _.numberOfColumns = 0;
        _.parent = null;
        
        
    }
    
    $.fn.resizableColumns = function (options) {
        
            return this.each(function () {

                widget = new Widget(this, options);

            });

        };
    
    $.fn.resizableColumns.destroy = function () {
        
        widget.destroy();
        
    }  
    
    
    $(window).resize(function () {
        
        widget.updateSize();
        var options, parent;
        
        if (widget.width < widget.settings.breakPoint && widget.horizontal) {
            
            options = widget.settings,
            parent = widget.parent;
            widget.destroy();
            widget = new Widget(parent, options);
            
        }
        
        if (widget.width > widget.settings.breakPoint && !widget.horizontal) {
            
            options = widget.settings,
            parent = widget.parent;
            widget.destroy();
            widget = new Widget(parent, options);
            
        }
        
        if (widget.horizontal) {
            
            widget.updateColumnWidth();
            
        } else {
            
            widget.updateLineHeight();
            
        }
        
        //only update max size on end of resize for performance
        clearTimeout(resizeEnd);
        resizeEnd = setTimeout(function () {
            
            if (widget.horizontal) {
                
                widget.updateMaxColumnWidth();
                
            } else {
                
                widget.updateMaxLineHeight();
                
            }
            
        }, 250);
        
    })
        
}( jQuery ));